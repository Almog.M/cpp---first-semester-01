#include <iostream>
#include <stdlib.h>
#include "queue.h"

/*
Initializes an unsigned int arr.
Input: the array, the size of the array.
Output: none.
*/
void initArr(unsigned int* arr, unsigned int size)
{
	unsigned int i = 0;
	if (arr)
	{
		for (i = 0; i < size; i++)
		{
			arr[i] = 0;
		}
	}
}

/*
Inits the queue dependent on the size of the queue.
Input: pointer to a queue, size of items to size the queue.
Output: none.
*/
void initQueue(queue* q, unsigned int size)
{
	if (q)
	{
		q->size = size;
		q->items = new unsigned int[size];
		initArr(q->items, size);
		q->nextEntryIndex = 0;
	}
}
/*
Cleans the queue - zeros all the values and release array.
We are assuming that the queue is in the heap memory...So we delete the q variable.
Input: pointer to a queue.
Output: none.
*/
void cleanQueue(queue* q)
{
	if (q)
	{
		q->size = 0;
		q->nextEntryIndex = 0;
		initArr(q->items, q->size);
		delete[] q->items;
		delete(q);
	}
}
/*
Add an item to the end of the queue.
Input: pointer to a queue, new item.
Output: none.
*/
void enqueue(queue* q, unsigned int newValue)
{
	if (q)
	{
		if (q->items)
		{
			if (q->nextEntryIndex < q->size)
			{
				q->items[q->nextEntryIndex] = newValue;
				q->nextEntryIndex++;
			}
		}
	}
}
/*
return element in top of queue, or -1 if empty.
Input: pointer to a queue.
Output: value of the item that we need to out.
*/
int dequeue(queue* q)
{
	int outItem = -1; //The item that we out from the queue.
	int i = 0;
	if (q && q->items)
	{
		if (QUEUE_HEAD_INDEX < q->nextEntryIndex)
		{
			outItem = (int)q->items[QUEUE_HEAD_INDEX];

			//Doing the deleting - remove each value one step to the head direction.
			for (i = 0; (unsigned int)i < q->size - 1; i++)
			{
				q->items[i] = q->items[i+1];
			}
			q->items[i] = 0; //The last item in the queue is empty now.

			q->nextEntryIndex--;//Decreasing the future index of the placement to the queue, 'cause one element has removed...
		}
	}
	return outItem;
}
/*
Prints the queue items in from the head of the queue.
Input: the queue.
Output: none.
*/
void printQueue(queue* q)
{
	int i = 0;
	for (i = 0; (unsigned int)i < q->size; i++)
	{
		std::cout << " " << q->items[i] << " ";
	}
	std::cout << " " << std::endl << " " << std::endl;
}
