#ifndef QUEUE_H
#define QUEUE_H

//Constants:
#define QUEUE_HEAD_INDEX 0//The index of the first element in the queue - index 0.

/* A queue contains positive integer values. */
typedef struct queue
{
	unsigned int* items;
	unsigned int size;
	unsigned int nextEntryIndex; //The index that a new item suppose to entries.
} queue;


void initQueue(queue* q, unsigned int size);
void cleanQueue(queue* q); //Include deleting array...
void initArr(unsigned int* arr, unsigned int size);



void enqueue(queue* q, unsigned int newValue);
int dequeue(queue* q); // return element in top of queue, or -1 if empty
void printQueue(queue* q); //For debugging - (:

#endif /* QUEUE_H */
