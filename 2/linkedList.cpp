//Almog Maman.
#include <stdio.h>
#include "linkedList.h"

/*
Add a node to the head of a list.
Input: head node, new node.
Output: the new head.
*/
node* addToHead(node* head, node* newNode)
{
	if(head != NULL)
	{
		head->pre = newNode;
		newNode->pre = NULL;
		newNode->next = head;
	}
	head = newNode;

	return head;
}
/*
Removes a node from a the top of a list.
Input: head of list.
Output: new head. 
*/
node* removeFromHead(node* head)
{
	node* temp = NULL;
	if (head != NULL) //If the list is not empty.
	{
		temp = head;
		if (head->next == NULL) //If we have just one node in the list.
		{
			head = NULL;
		}
		else //If we have two or more nodes...
		{
			head = head->next;
			head->pre = NULL;
		}
		delete(temp);
	}
	return head;
}
/*
Creates a node.
Input: value.
Output: dynamic memory.
*/
node* createNode(unsigned int value)
{
	node* newNode = new node;
	newNode->next = NULL;
	newNode->pre = NULL;
	newNode->value = value;
	return newNode;
}

