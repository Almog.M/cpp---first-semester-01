//Almog Maman.
#pragma once
/*Element node - the node in the stack object.*/
typedef struct node
{
	unsigned int value;
	node* pre;
	node* next;
}node;



node* addToHead(node* head, node* newNode);
node* removeFromHead(node* head);
node* createNode(unsigned int value);
