//Almog Maman.
#include "linkedList.h"
#include "stack.h"
#include <stdio.h>

/*
Entries a value into the stack as node.
Input: pointer to stack, element to enter.
Output: none.
*/
void push(stack* s, unsigned int element)
{
	if (s)
	{
		node* newNode = createNode(element);
		s->head = addToHead(s->head, newNode);
	}
}
/*
Removes the head of the stack.
Input: stack.
Output: the values of the head that deleted or -1 if the stack is empty.
*/
int pop(stack* s)
{
	int value = -1;
	if (s)
	{
		if (s->head != NULL)
		{
			value = (int)s->head->value;
			s->head = removeFromHead(s->head);
		}
	}
	return value;
}
/*
Inits the stack to default value.
Input: the stack.
Output: none.
*/
void initStack(stack* s)
{
	if (s)
	{
		s->head = NULL;
	}
}
/*
Cleans the stack - in the dynamic memory case and values case.
We are assuming that the stack that we have created is in the heap memory...So we delete the s variable.
Input: pointer to stack.
Output: None.
*/ 
void cleanStack(stack* s)
{
	node* temp = NULL;

	if (s)
	{
		while (s->head)
		{
			temp = s->head;
			s->head = s->head->next;

			temp->pre = NULL;
			temp->next = NULL;
			temp->value = 0;
			delete(temp);
		}
		delete(s);
	}
}