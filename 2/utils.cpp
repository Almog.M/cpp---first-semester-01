//Almog Maman.
#include "utils.h"
#include <stdio.h>
#include "linkedList.h"
#include "stack.h"
#include <iostream>

/*
Reverse an int array by stack.
Input: int arr, its size.
Output: none.
*/
void reverse(int* nums, unsigned int size)
{
	int i = 0;
	stack* s = new stack;
	node* originalHead = NULL; //The original head of the stack after charging, for delete the stack.

	initStack(s);

	//Charging the stack - doing push...
	for (i = 0; (unsigned int)i < size; i++)
	{
		push(s, (unsigned int)nums[i]);
	}
	originalHead = s->head;

	//Revers the array:
	//std::cout << "The swapped array " << std::endl; //message for the swapped array.
	i = 0;
	while (s->head)
	{
		nums[i] = (int)s->head->value;
		//std::cout << "" << nums[i] << " "; //for seeing the swapped array.

		s->head = s->head->next;
		i++;
	}

	//Cleaning...
	s->head = originalHead; //For removing the whole stack.
	cleanStack(s);
}
/*
Swaps 10 cells int array.
Input: none.
Output: the swapped array.
*/
int* reverse10()
{
	int i = 0;
	int* arr = new int[REVERSE_ARR_SIZE];

	std::cout << "Get  int "<< REVERSE_ARR_SIZE << " numbers please." << std::endl;

	for (i = 0; i < REVERSE_ARR_SIZE; i++)
	{
		std::cin >> arr[i];
		getchar();
	}
	reverse(arr, REVERSE_ARR_SIZE);

	
	return arr;
}
